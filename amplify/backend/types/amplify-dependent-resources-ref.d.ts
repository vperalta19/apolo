export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "apolo": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string"
        }
    }
}