/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import LambdaManager from './src/controllers/LambdaManager'

AppRegistry.registerComponent(appName, () => App);


