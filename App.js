import Voice from './src/screens/Voice';
import Chat from './src/screens/Chat';
import Bottom from './src/components/Bottom';

import React, { Component } from 'react'
import LexManager from './src/controllers/LexManager';
import LambdaManager from './src/controllers/LambdaManager';
import SplashScreen from 'react-native-splash-screen';
import TermsAndConditions from './src/screens/TermsAndConditions';

export default class App extends Component {
	constructor(){
		super();
		this.state = {
			screen: 'voice',
			messages: [],
			lastApolo: {},
			lastPaciente: {},
			loading: true
		}
		this.message = {};
		this.listener = LexManager.addListener(async (message)=>{
			if(message !== this.message){
				this.message = message;
				await this.updateMessages();
				this.newMessage(message);
			}
		});
	}

	componentDidMount(){
		SplashScreen.hide();
		this.setState({loading: true})
		LambdaManager.postUser(
			()=>{
				LexManager.putSession(()=>{
					LexManager.checkExpiredChats();
					setTimeout(() => {
						this.setState({loading: false})
					}, 10000);
				})
			}
		);
	}

	componentWillUnmount(){
		LexManager.removeListener(this.listener);
	}

	newMessage(message){
		if(message.type === 'apolo'){
			if(message.expectsAnswer){
				this.setState({lastApolo: message, lastPaciente: {}})
			}
			else{
				this.setState({lastApolo: message});
			}
		}
		else if(message.type === 'paciente'){
			this.setState({lastPaciente: message})
		}
		else if(message.type === 'remove'){
			this.setState({lastApolo: message.lastApolo, lastPaciente: {}})
		}
	}

	async updateMessages(){
		let messages = await LexManager.getMessages();
		if(messages === null || messages.length === 0){
			await LexManager.addMessage('apolo', 'Hola! Soy Apolo, tu asistente médico personal. ¿En qué te puedo ayudar?');
		}
		this.setState({messages: messages !== null ? messages : []})
	}

	setScreen(screen){
		this.setState({
			screen: screen
		})
	}

	render() {
		return (
			<>	
				{
					(this.state.loading) ?
					<TermsAndConditions></TermsAndConditions>
					:
					<>
						{(this.state.screen === 'voice') ? 
							<Voice lastPaciente={this.state.lastPaciente} lastApolo={this.state.lastApolo}></Voice> 
							: 
							<Chat messages={this.state.messages} lastApolo={this.state.lastApolo}></Chat>}
						<Bottom screenOpen={this.state.screen} setScreen={(s)=>this.setScreen(s)}></Bottom>
					</>					
				}
				
			</>
		)
	}
}

