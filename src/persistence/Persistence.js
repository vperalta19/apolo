import AsyncStorage from '@react-native-async-storage/async-storage';

export const getStringValue = async (key) => {
    try {
        const value = await AsyncStorage.getItem(key);
        return (value != null || value != undefined) ? value : null;
    } catch(e) {
        // read error
    }
}

export const getObjectValue = async (key) => {
	try {
		const jsonValue = await AsyncStorage.getItem(key);
		return (jsonValue != null || jsonValue != undefined) ? JSON.parse(jsonValue) : null;
	} catch(e) {
		console.error(e);
		// read error
	}
}

export const setStringValue = async (key,value) => {
	try {
		await AsyncStorage.setItem(key, value);
	} catch(e) {
		console.error(e);
		// save error
	}
}

export const setObjectValue = async (key, value) => {
	try {
		const jsonValue = JSON.stringify(value);
		await AsyncStorage.setItem(key, jsonValue);
	} catch(e) {
		console.error(e);
		// save error
	}
}

export const removeValue = async (key) => {
    try {
        await AsyncStorage.removeItem(key);
    } catch(e) {
        // remove error
    }
}

export const clearAll = async () => {
    try {
        await AsyncStorage.clear()
    } catch(e) {
        // clear error
    }
}
