import AWS from 'aws-sdk/dist/aws-sdk-react-native'
import DeviceInfo from 'react-native-device-info';
import LambdaConfigurations from '../config/LambdaConfigurations'
import { setStringValue } from '../persistence/Persistence';

class LambdaManager{
    constructor(){
        this.lambdaSDK = new AWS.Lambda(LambdaConfigurations.lambdaClassParams);
    }

    post(params,callback){
        this.lambdaSDK.invoke({...LambdaConfigurations.lambdaFunctionParams,Payload: JSON.stringify(params)},function(err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else{// successful response
                console.log(data)
                callback()
            };           
        })
    }

    postMessage(message,sessionId, sender,callback){
        this.post({
            endpoint: 'postMessage',
            message: message,
            sessionId: sessionId,
            sender: sender
        },callback)
    }

    async getUniqueId(){
        return await DeviceInfo.getUniqueId();
    }
    
    async postUser(callback){
        const uniqueIdValue = await this.getUniqueId();
        this.post({
            endpoint: 'postUser',
            uniqueId: uniqueIdValue
        },callback)
        setStringValue('user',uniqueIdValue);
        
    }
    
    async postSession(sessionId,callback){
        const uniqueIdValue = await this.getUniqueId();
        this.post({
            endpoint: 'postSession',
            sessionId: sessionId,
            uniqueId: uniqueIdValue
        },callback)
        setStringValue('sessionId',sessionId)
    }


}
export default new LambdaManager();