import ChangeListenerMixin from '../mixins/ChangeListenerMixin';
import { getObjectValue, removeValue, setObjectValue } from '../persistence/Persistence';
import AWS from 'aws-sdk/dist/aws-sdk-react-native'
import removeAccents from 'remove-accents'
import LexConfigurations from '../config/LexConfigurations';
import LambdaManager from './LambdaManager';

class LexManager {
    constructor(){
        this.changeListenerMixin = ChangeListenerMixin;
        this.listenersId = [];
        
    }

    async resetMessages(){
        await setObjectValue('messages',{});
    }

    createLexInstance(){
        return new AWS.LexRuntimeV2(LexConfigurations.lexRunTimeV2);
    }

    createSessionId(){
        return ((new Date()).getTime()).toString();
    }

    getSessionLexParams(){
        return {
            ...LexConfigurations.sessionInfo,
            sessionId: this.sessionId
        }
    }

    getSessionId(){
        return this.sessionId;
    }

    putSession(callback){
        this.sessionId = this.createSessionId();
        this.lexRunTime = this.createLexInstance();
        this.lexRunTime.putSession(this.getSessionLexParams());
        LambdaManager.postSession(this.sessionId,callback);
    }

    async send(message){
		await this.addMessage('paciente', message);
        await this.addMessage('apolo', LexConfigurations.loadingMessage, false);
        try {
            this.handleTextSubmit(message);
        } catch (error) {
            console.log(error);
        }
	}

    async handleTextSubmit(message) {
        const self = this;
        if (message !== ''){
            let params = this.getSessionLexParams();
            params['text'] = removeAccents.remove(message);

            await this.lexRunTime.recognizeText(params, async (err, data) => {
                await this.removeLastMessage();
                if(err) {
                    console.log('error' + err);
                    const errorLambdaMessage = LexConfigurations.errorLambdaMessage;
                    self.addMessage('apolo', errorLambdaMessage);
                }
                if(data){
                    if (data.messages) {
                        self.addMessage('apolo', data.messages[0].content, !(data.sessionState && data.sessionState.dialogAction.type === 'Close'));
                    }
                    else if(data.sessionState && data.sessionState.dialogAction.type === 'Close'){
                        const closeMessage = LexConfigurations.closeMessage;
                        self.addMessage('apolo', closeMessage, false);
                    }
                }
            })
        }  
    }

    async removeMessages(){
        await removeValue('messages');
        this.listenerOnChange({});
    }

    async removeLastMessage(){
        let messages = await getObjectValue('messages');
        messages.pop();
        setObjectValue('messages',messages);
    }

    async addMessage(type, message, expectsAnswer = true){
        const self = this;
        if(type === 'apolo' && message !== LexConfigurations.loadingMessage){
            LambdaManager.postMessage(message,this.getSessionId(),0,async ()=>{
               await self._addMessage(type, message, expectsAnswer);
            });
        }
        else{
            await this._addMessage(type, message, expectsAnswer);
        }
	}

    async _addMessage(type, message, expectsAnswer){
        if(message !== undefined){
			let messages = await getObjectValue('messages');
            if(messages === null){
                messages = [];
            }
            message = {type: type, message: message, expectsAnswer: expectsAnswer, expirationDate: this.getExpirationDate()};
			messages.push(message);
			setObjectValue('messages',messages);
            this.listenerOnChange(message);
		}
    }

    getExpirationDate(){
        let date = new Date();
        date.setHours(date.getHours() + LexConfigurations.expiredTime);
        return date.getTime();
    }

    async getMessages(){
        let messages = await getObjectValue('messages');
        return messages;
	}

    listenerOnChange(message){
        this.listenersId.forEach((id)=>{
            this.changeListenerMixin.onChange(id,message);
        });
    }

    addListener(callback) { 
        const id = this.changeListenerMixin.addListener(callback);
        this.listenersId.push(id);
        return id;
    }

    removeListener(id){
        this.changeListenerMixin.removeListener(id);
    }

    removeAllListeners(){
        this.changeListenerMixin.removeAllListeners();
    }

    removeExpiredMessage(messages,index){
        const dateNow = (new Date()).getTime();
        return(parseInt(messages[index].expirationDate) < parseInt(dateNow))
    }

    async checkExpiredChats(){
        let messages = await this.getMessages();
        let flag = false;
        if(messages !== null && messages.length > 0){
            while(messages.length > 0){
                const deleteExpiredMessage = this.removeExpiredMessage(messages,0);
                if(deleteExpiredMessage) {
                    flag = true;
                    messages.shift();
                }
                else break;
            }
            if(flag){
                await setObjectValue('messages', messages);
            }
            this.onRemove(messages)
        }
        else{
            await setObjectValue('messages', []);
            this.onRemove([])
        }
        
    }

    onRemove(messages){
        if(messages.length === 0){
            this.listenerOnChange({type: 'remove', lastApolo: {}, lastPaciente: {}})
        }
        else{
            let lastApolo = null;
            let lastPaciente = null;
            for(let i = messages.length - 1; i >= 0 && (lastApolo === null || lastPaciente === null); i--){
                if(messages[i].type === 'apolo' && lastApolo === null){
                    lastApolo = messages[i];
                }
                else if(messages[i].type === 'paciente' && lastPaciente === null){
                    lastPaciente = messages[i];
                }
            }
            this.listenerOnChange({
                type: 'remove', 
                lastApolo: lastApolo !== null ? lastApolo : {}, 
                lastPaciente: lastPaciente !== null ? lastPaciente : {}
            })
        }
    }
}

export default new LexManager();