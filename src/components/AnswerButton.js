import { StyleSheet, Text, View } from 'react-native'
import React, { Component } from 'react'
import ButtonComponent from './ButtonComponent'

export default class AnswerButton extends Component {
  render() {
    return (
        <View style={styles.answer}>
            {
                (
                    this.props.lastApolo == {} || 
                    !this.props.lastApolo.expectsAnswer ||
                    this.props.isListening
                ) ? <></> :
                <ButtonComponent backgroundColor='white' size='large' 
                                onClick={()=>{this.props.startRecognizing()}} 
                                text='Contestar'>
                </ButtonComponent>
            }
        </View>
    )
  }
}


const styles = StyleSheet.create({
	answer:{
		width: '100%',
		alignContent: 'center',
		backgroundColor: 'white',
		paddingHorizontal: 25,
		height: 50
	}
})