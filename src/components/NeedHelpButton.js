import { StyleSheet, Text, View } from 'react-native'
import React, { Component } from 'react'
import LexManager from '../controllers/LexManager'
import ButtonComponent from './ButtonComponent'

export default class NeedHelpButton extends Component {
  render() {
    return (
        <View style={styles.needHelp}>
            {
                <ButtonComponent backgroundColor='white' size='large' 
                                onClick={()=>{LexManager.send('Necesito ayuda!')}} 
                                text='Necesito ayuda!'>
                </ButtonComponent>
            }
        </View>
    )
  }
}

const styles = StyleSheet.create({
	needHelp:{
		width: '100%',
		alignContent: 'center',
		backgroundColor: 'white',
		paddingHorizontal: 25,
		height: 50,
	}
})