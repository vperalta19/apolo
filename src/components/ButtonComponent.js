import { StyleSheet, Text, View, Image, TouchableNativeFeedback } from 'react-native'
import React, { Component } from 'react'

export default class ButtonComponent extends Component {
	constructor(props){
		super(props)
	}
	render() {
		return (
			<TouchableNativeFeedback onPress={this.props.onClick}>
				<View style={[buttonContainer[this.props.size],buttonContainer[this.props.backgroundColor]]}>
					{
						(this.props.image !== undefined) ?
						<Image
							style={styles.image}
							source={this.props.image}
						/> : <></>
					}
					{
						(this.props.size === 'large') ? 
						<Text style={buttonText[this.props.backgroundColor]}>
							{this.props.text}
						</Text> : 
						<></>
					}
				</View>
			</TouchableNativeFeedback>
		)
	}
}

const styles = StyleSheet.create({
	buttonContainer:{
		display: 'flex',
		flexDirection: 'row',
		borderRadius: 20,
		padding: 8,
		alignItems: 'center',
		justifyContent: 'center',
		margin: 5
	},
    image:{
		width: 16,
		height: 15
	},
	buttonText:{
		fontFamily: 'Nunito-SemiBold',
        fontSize: 20,
        lineHeight: 27,
        textAlign: 'center',
        color: '#F2F2EC',
		height: 30,
		marginLeft: 5
	}
})

const buttonText = StyleSheet.create({
	white:{
		...styles.buttonText,
		color: '#B40000'
	},
	black: {
		...styles.buttonText,
		color: '#F2F2EC'
	},
})

const buttonContainer = StyleSheet.create({
	large:{
		...styles.buttonContainer,
		flex: 10
		
	},
	short: {
		...styles.buttonContainer,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
		flex: 1
	},
	white:{
		backgroundColor: '#F2F2EC',
	},
	black:{
		backgroundColor: '#3E322D',
	},
})