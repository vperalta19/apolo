

import {
    Text,
    StyleSheet,
    View,
    ScrollView,
    Image
} from 'react-native'

export default function Message(props) {
    
    return (
        <View style={bubbleChatContainer[props.type]}>
            <View style={bubbleChat[props.type]}>
                {(props.text !== undefined && props.text.length > 75 && props.scroll !== undefined) ? 
                <ScrollView style={styles.scroll}>
                    <Text style={messageTextWithScroll[props.type]}>{props.text}</Text> 
                </ScrollView>
                :
                (props.text == undefined) ?
                <Image
                    style={styles.image}
                    source={require('../assets/img/listening.png')}
                />
                :
                <Text style={messageText[props.type]}>{props.text}</Text> 
                }
            </View>
            <View>
                <View style={triangle[props.type]}></View>
            </View>
        </View>
        
    )
}

const styles = StyleSheet.create({
    bubbleChatContainer:{
        padding: 20,
    },
    bubbleChat:{
        borderRadius: 10,
    },
    triangle:{
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderBottomWidth: 0,
        borderRightWidth: 12,
        borderTopWidth: 12,
        borderLeftWidth: 12,
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
    },
    messageText:{
        fontFamily: 'Nunito-ExtraLight',
        fontSize: 20,
        lineHeight: 27,
        textAlign: 'left',
        padding: 10,
        paddingHorizontal: 20,
    },
    messageTextWithScroll:{
        fontFamily: 'Nunito-ExtraLight',
        fontSize: 20,
        lineHeight: 27,
        textAlign: 'left',
        paddingHorizontal: 20,
    },
    scroll:{
        margin: 10,
    },
    image:{
        margin: 10,
        marginHorizontal: 25,
		width: 150,
		height: 30
	},
})

const bubbleChatContainer = StyleSheet.create({
    paciente:{
        ...styles.bubbleChatContainer,
        alignSelf: 'flex-end'
        
    },
    apolo:{
        ...styles.bubbleChatContainer,
        alignSelf: 'flex-start'
    }
})

const messageText = StyleSheet.create({
    paciente:{
        ...styles.messageText,
        color: '#3E322D',
        
    },
    apolo:{
        ...styles.messageText,
        color: '#F2F2EC',
    }
})

const messageTextWithScroll = StyleSheet.create({
    paciente:{
        ...styles.messageTextWithScroll,
        color: '#3E322D',
        
    },
    apolo:{
        ...styles.messageTextWithScroll,
        color: '#F2F2EC',
    }
})

const bubbleChat = StyleSheet.create({
    paciente:{
        ...styles.bubbleChat,
        backgroundColor: '#F2F2EC',
        
    },
    apolo:{
        ...styles.bubbleChat,
        backgroundColor: '#B40000',
    }
})

const triangle = StyleSheet.create({
    paciente:{
        ...styles.triangle,
        borderTopColor: '#F2F2EC',
        position: 'absolute',
        right: 10
        
    },
    apolo:{
        ...styles.triangle,
        borderTopColor: '#B40000',
        position: 'absolute',
        left: 10
    }
})