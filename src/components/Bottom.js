import { StyleSheet, View } from 'react-native'
import React, { useState, Component } from 'react';
import ButtonComponent from './ButtonComponent'

export default class Bottom extends Component {
	constructor(){
		super();
	}

	openScreen(screen){
		this.props.setScreen(screen);
	}

	render() {
		return (
			<View style={styles.bottomContainer}>
				<ButtonComponent backgroundColor='white' text='Voz' onClick={()=>this.openScreen('voice')} 
								image={require('../assets/img/voice.png')}
								size={(this.props.screenOpen === 'voice') ? 'large' : 'short'} >
				</ButtonComponent>
				<ButtonComponent backgroundColor='black' text='Texto' onClick={()=>this.openScreen('chat')} 
								image={require('../assets/img/chat.png')}
								size={(this.props.screenOpen === 'chat') ? 'large' : 'short'} >
				</ButtonComponent>
			</View>
		)
	}
}


const styles = StyleSheet.create({
	bottomContainer:{
		padding: 5,
		display: 'flex',
		flexDirection: 'row',
		backgroundColor: '#FFFFFF'
	}
})