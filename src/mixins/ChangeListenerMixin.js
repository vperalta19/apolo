/*
    Mixin que provee la funcionalidad para escuchar cambios en un objeto
*/
class ChangeListenerMixin {
    constructor() {
        this.getListeners = function() {
            if (this._listeners == null) this._listeners = {};
            return this._listeners;
        };
        this.onChange = function(id,params) {
            if(id){
                const listenerCallback = this.getListeners()[id]
                if(listenerCallback){
                    listenerCallback(params);
                }
                
            }
        };
        this.addListener = function(callback,name) {
            let id = Date.now();
            if(name != null){
                id = name;
            }
            this.getListeners()[id] = callback;
            return id;
        };
        this.removeListener = function(id) {
            delete this.getListeners()[id];
        };
        this.removeAllListeners = function(){
            this._listeners = null
        }
    }
}

export default new ChangeListenerMixin();