import { StyleSheet, View, Image, ScrollView, TextInput, KeyboardAvoidingView } from 'react-native'
import Message from '../components/Message'
import ButtonComponent from '../components/ButtonComponent';
import LexManager from '../controllers/LexManager';

import React, { Component } from 'react'
import NeedHelpButton from '../components/NeedHelpButton';

export default class Chat extends Component {
	constructor(props){
		super(props);
		this.scrollViewRef = React.createRef();
		this.state = {
			message: ''
		}
	}

	async send(){
		await LexManager.send(this.state.message);
		this.setMessage('');
	}

	setMessage(message){
		this.setState({
			message: message
		})
	}


	render() {
		return (
			<>
				<KeyboardAvoidingView style={styles.screen}>
					<View style={styles.header}>
						<Image
							style={styles.image}
							source={require('../assets/img/eyes.png')}
						/>
					</View>
					<View style={styles.body}>
						<ScrollView 
							style={styles.chat}
							ref={this.scrollViewRef}
							onContentSizeChange={() => this.scrollViewRef.current.scrollToEnd({ animated: true })}
						>
							{this.props.messages.map((message,key)=>{
								return 	<Message type={message.type} text={message.message} key={key}></Message>
							})}
						</ScrollView>
						{
							(this.props.lastApolo == {} || 
								this.props.lastApolo.expectsAnswer || 
								this.props.lastApolo.message == 'Dame un segundo...') 
							?
							<></>
							:
							(Object.keys(this.props.lastApolo).length > 0)
							?
							<NeedHelpButton lastApolo={this.props.lastApolo}></NeedHelpButton>
							:
							<></>
						}
						<View style={styles.inputContainer}>
							<TextInput 
								style = {styles.input}
								placeholder = "Mensaje"
								onChangeText={(m)=>this.setMessage(m)}
								value={this.state.message}
							>
							</TextInput>
							<ButtonComponent backgroundColor='white' size='short' onClick={()=>this.send()} 
												image={require('../assets/img/send.png')}>
							</ButtonComponent>
						</View>
					</View>
				</KeyboardAvoidingView>
			</>
		)
	}
}

const styles = StyleSheet.create({
	actionButton:{
		width: '100%',
		alignContent: 'center',
		backgroundColor: 'white',
		paddingHorizontal: 25,
		height: 50
	},
	screen:{
		flex: 1,
		backgroundColor: '#FFFFFF',
		display: 'flex',
		paddingBottom: 25
	},
	header:{
		backgroundColor: '#3E322D',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		flex: 1
	},
	image:{
		width: '50%',
		height: 29,
	},
	body:{
		flex: 10,
		height: '100%'
	},
	chat:{
		overflow: 'scroll',
	},
	inputContainer:{
		display: 'flex',
		flexDirection: 'row',
		paddingHorizontal: 20
	},
	input:{
		backgroundColor: '#F2F2EC',
		borderRadius: 10,
		width: '90%',
		height: 50,
		alignSelf: 'center',
		fontFamily: 'Nunito-Light',
        fontSize: 20,
        lineHeight: 27,
		color: '#3E322D',
		selectionColor: '#3E322D',
		flex: 11
	}
})