import { Image, StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native'
import React, { Component } from 'react'
import Message from '../components/Message'
import Tts from 'react-native-tts';
import LexManager from '../controllers/LexManager';
import Voice from '@react-native-voice/voice';
import NeedHelpButton from '../components/NeedHelpButton';

Tts.setDefaultLanguage('es-US');
Tts.setDefaultVoice('es-us-x-esd-network');


export default class VoicePage extends Component {
	constructor(props){
		super(props);
		this.subscribeListeners();
		this.state = {
			error: false,
			listening: false,
			speaking: false,
			messagesToSay: [],
			lastApolo: props.lastApolo
		}
		this.errorSpeech = 0;
	}

	//TEXT TO SPEECH
    speak(message){
        Tts.speak(message);
		this.setState({speaking: true});
    }

    onStopSpeaking(){
		this.setState({speaking: false});
		if(this.state.lastApolo.expectsAnswer && !this.isListening()){
			this.startRecognizing();
			this.setState({messagesToSay: []});
		}
		else{
			this.sayLastMessageInQueue();
		}
	}
    

    //SPEECH RECOGNITION
	isListening(){
        return this.state.listening;
    }
    onSpeechError = (e) => {
		this.errorSpeech += 1;
		this.setState({error: true})
		if(e.error.message === '7/No match'){
			this.startRecognizing();
		}
		else{
			this.resetRecognizer();
			let message = ''
			if(this.errorSpeech > 2){
				message = 'Tengo problemas para escucharte, por favor, inténtalo a través del chat, o comunícate con el número de emergencias local.'
				LexManager.addMessage('apolo', message, false);
			}
			else{
				message = 'Ups, tuve un error al intentar escucharte. ¿Puedes intentarlo de nuevo?'
				LexManager.addMessage('apolo', message);
			}	
		}
    };
	
	onSpeechResults = (e) => {
		this.stopTimer();
		this.sendMessage(e.value[0]);
    };

	onSpeechPartialResults = (e) => {
		this.checkSpeech(e.value[0]);
    };

	checkSpeech(partials){
		if(partials != ''){
			this.stopTimer();
			this.partialsResult = partials;
			this.startTimer();
		}
	}

	stopTimer(){
		if(this.partialsTimer != null){
			clearTimeout(this.partialsTimer);
			this.partialsTimer = null;
		}
	}

	startTimer(){
		this.partialsTimer = setTimeout(()=>{
			if(this.partialsResult != ''){
				this.sendMessage(this.partialsResult);
			}
			this.stopTimer();
		}, 4000)
	}
	

    startRecognizing = async () => {
        try {
            await Voice.start('es-US');
			this.setState({listening: true, error: false});
        } catch (e) {
            console.error(e);
        }
    };
    
    destroyRecognizer = async () => {
        try {
            await Voice.destroy();
        } catch (e) {
            console.error(e);
        }
    };

    resetRecognizer(){
		this.destroyRecognizer();
		this.setState({
			error: false,
            listening: false
		});
		this.partialsResult = '';
	}

    //VOICE LOGIC
    async componentDidMount(){
		this.subscribeListeners();
		this.lexListener = LexManager.addListener((m)=>{
            if(m.type === 'apolo'){
				this.addApoloMessage(m)
            }
			else if(m.type === 'remove'){
				this.setState({lastApolo: m.lastApolo})
			}
        });
		if(Object.keys(this.state.lastApolo).length > 0 && this.state.lastApolo.expectsAnswer){
			this.sayLastApolo();
		}
	}

	sayLastMessageInQueue(){
		let messagesToSay = [...this.state.messagesToSay];
		if(messagesToSay.length > 0){
			const message = messagesToSay.shift();
			this.setState({lastApolo: message, messagesToSay: messagesToSay});
			this.speak(message.message);
		}
	}

	addApoloMessage(message){
		if(this.state.speaking || this.state.messagesToSay.length > 0){
			let messagesToSay = [...this.state.messagesToSay];
			messagesToSay.push(message);
			this.setState({
				messagesToSay: messagesToSay
			})
		}
		else{
			this.setState({
				lastApolo: message
			})
			this.speak(message.message);
		}
	}

    componentWillUnmount() {
		Voice.destroy().then(Voice.removeAllListeners);
		LexManager.removeListener(this.lexListener);
		Tts.removeAllListeners('tts-finish');
	}

    subscribeListeners(){
		Voice.onSpeechError = this.onSpeechError.bind(this);
		Voice.onSpeechResults = this.onSpeechResults.bind(this);
		Voice.onSpeechPartialResults = this.onSpeechPartialResults.bind(this);

		Tts.removeAllListeners('tts-finish');
		Tts.addListener('tts-finish', () => this.onStopSpeaking());
	}

    sendMessage(message){
		this.resetRecognizer();
		this.errorSpeech = 0;
		if(message != ''){
			LexManager.send(message);
		}
	}

	sayLastApolo(){
		this.speak(this.state.lastApolo.message)
	}

	render() {
		return (
			<>
				<View style={styles.screen}>
					<View style={styles.messageContainer}>
						{
							(this.state.lastApolo.message !== undefined) 
							?
							<Message type='apolo' text={this.state.lastApolo.message} scroll={true}></Message> 
							:
							''
						}
					</View>
					<View>
						<TouchableWithoutFeedback onPress={()=>this.sayLastApolo()}>
							<Image
								style={styles.image}
								source={require('../assets/img/apolo.png')}
							/>
						</TouchableWithoutFeedback>
					</View>
					<View style={styles.messageContainer}>
						{
							(
								Object.keys(this.state.lastApolo).length == 0 || 
								this.state.lastApolo.expectsAnswer || 
								this.state.lastApolo.message == 'Dame un segundo...'
							) 
							? 
							(this.props.lastPaciente.message !== undefined || this.state.listening) 
							?
							<Message type='paciente' text={this.props.lastPaciente.message} scroll={true} error={this.state.error}></Message> 
							:
							<></>
							:
							(!this.state.speaking && Object.keys(this.state.lastApolo).length > 0) 
							?
							<NeedHelpButton></NeedHelpButton>
							: 
							<></>
						}
					</View>
				</View>
			</>
		)
	}
}


const styles = StyleSheet.create({
	screen:{
		flex: 1,
		backgroundColor: '#FFFFFF',
		display: 'flex',
	},
	messageContainer:{
		flex: 1,
		justifyContent: 'center'
	},
	image:{
		width: 200,
		height: 300,
		alignSelf: 'center',
	},
	actionButton:{
		width: '100%',
		alignContent: 'center',
		backgroundColor: 'white',
		paddingHorizontal: 25,
		height: 50
	}
})