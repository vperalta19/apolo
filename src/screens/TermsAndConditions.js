import { Image, StyleSheet, Text, View } from 'react-native'
import React, { Component } from 'react'

export default class TermsAndConditions extends Component {
  render() {
    return (
        <View style={styles.screen}>
            <Image
                style={styles.image}
                source={require('../assets/img/apolo_terms.png')}
            />      
        </View>
    )
  }
}

const styles = StyleSheet.create({
	screen:{
		flex: 1,
		backgroundColor: '#FFFFFF',
		display: 'flex',
        justifyContent: 'center'
	},
	image:{
		width: 250,
		height: 500,
		alignSelf: 'center',
	},
})